<?php

use Faker\Generator as Faker;

use App\Models\Button;

$factory->define(Button::class, function (Faker $faker) {
    return [
        'title' => $faker->company,
    ];
});
