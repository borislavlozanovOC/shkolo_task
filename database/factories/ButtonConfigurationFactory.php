<?php

use Faker\Generator as Faker;

use App\Models\ButtonConfiguration;
use App\Models\Color;

$factory->define(ButtonConfiguration::class, function (Faker $faker) {
    return [
    	'color_id' => Color::all()->random()->id,
        'hyperlink' => $faker->url,
    ];
});
