<?php

use Faker\Generator as Faker;

use App\Models\Color;

$factory->define(Color::class, function (Faker $faker) {
    return [
        'safe_color_name' => $faker->unique()->safeColorName,
    ];
});
