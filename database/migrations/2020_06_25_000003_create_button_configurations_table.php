<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateButtonConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('button_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('button_id')->unsigned();
            $table->foreign('button_id')->references('id')->on('buttons')->onDelete('cascade');
            $table->integer('color_id')->unsigned();
            $table->foreign('color_id')->references('id')->on('colors')->onDelete('cascade');
            $table->string('hyperlink')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('button_configurations', function (Blueprint $table) {
            $table->dropForeign(['button_id']);
            $table->dropForeign(['color_id']);
        });

        Schema::dropIfExists('button_configurations');
    }
}
