<?php

use Illuminate\Database\Seeder;

class ButtonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Button::class, 9)->create()->each(function ($button) {
            $button->configuration()->save(
                factory(App\Models\ButtonConfiguration::class)->make()
            );
        });
    }
}
