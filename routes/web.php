<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'ButtonController@index']);

Route::resource('/buttons', 'ButtonController',
['names' => [
	'index' => 'index.button',
	'edit' => 'edit.button',
	'update' => 'update.button',
	'destroy' => 'destroy.button',
],'except' => [
    'create',
    'store',
    'show',
]]);