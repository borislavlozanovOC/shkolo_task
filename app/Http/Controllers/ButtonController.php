<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ButtonRequest;

use App\Repositories\ButtonRepositoryInterface;
use App\Repositories\ColorRepositoryInterface;

class ButtonController extends Controller
{
    private $buttonRepository;
    private $colorRepository;
    
    /**
     * ButtonController constructor.
     *
     * @param ButtonRepositoryInterface $buttonRepository
     * @param ColorRepositoryInterface $colorRepository
     */
    public function __construct(
        ButtonRepositoryInterface $buttonRepository,
        ColorRepositoryInterface $colorRepository
    ) {
       $this->buttonRepository = $buttonRepository;
       $this->colorRepository = $colorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('index')
                ->with('buttons', $this->buttonRepository->all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('form')
                ->with('button', $this->buttonRepository->find($id))
                ->with('colors', $this->colorRepository->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\ButtonRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Redirect
     */
    public function update(ButtonRequest $request, $id)
    {
        $this->buttonRepository->updateConfiguration($id, $request);
        
        return redirect()->route('index.button');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Redirect
     */
    public function destroy($id)
    {
        $this->buttonRepository->find($id)->configuration()->delete();

        return redirect()->route('index.button');
    }
}
