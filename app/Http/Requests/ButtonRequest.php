<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ButtonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3|max:100',
            'hyperlink' => 'required|url',
            'color_id' => 'required|exists:colors,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'A title is required',
            'title.min' => 'A title must be atleast 3 characters',
            'title.max' => 'A title must be maximum 100 characters',
            'hyperlink.required' => 'A hyperlink is required',
            'hyperlink.url' => 'A hyperlink must be a valid URL',
            'color_id.required' => 'A Color is required',
            'color_id.exists' => 'A Color is not available',
        ];
    }
}
