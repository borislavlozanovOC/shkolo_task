<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';

    public $timestamps = true;

    protected $fillable = [
        'safe_color_name',
        'created_at',
        'updated_at'
    ];

    public function configuration()
    {
        return $this->belongsToMany(ButtonConfiguration::class, 'color_id');
    }
}
