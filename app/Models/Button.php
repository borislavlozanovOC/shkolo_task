<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Button extends Model
{
    protected $table = 'buttons';

    public $timestamps = true;

    protected $fillable = [
        'title',
        'created_at',
        'updated_at'
    ];

    public function configuration()
    {
        return $this->hasOne(ButtonConfiguration::class, 'button_id');
    }
}
