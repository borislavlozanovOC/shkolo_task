<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ButtonConfiguration extends Model
{
    protected $table = 'button_configurations';

    public $timestamps = true;

    protected $fillable = [
        'button_id',
        'color_id',
        'hyperlink',
        'created_at',
        'updated_at'
    ];

    public function button()
    {
        return $this->belongsTo(Button::class, 'button_id');
    }

    public function color()
    {
        return $this->belongsTo(Color::class, 'color_id');
    }
}
