<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\EloquentRepositoryInterface;
use App\Repositories\ButtonRepositoryInterface;
use App\Repositories\ColorRepositoryInterface;

use App\Repositories\Eloquent\EloquentRepository;
use App\Repositories\Eloquent\ButtonRepository;
use App\Repositories\Eloquent\ColorRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
    * Bootstrap the application services.
    *
    * @return void
    */
    public function boot()
    {
        //
    }

    /**
    * Register the application services.
    *
    * @return void
    */
    public function register()
    {
        $this->app->bind(
            'App\Repositories\EloquentRepositoryInterface',
            'App\Repositories\Eloquent\EloquentRepository'
        );
        $this->app->bind(
            'App\Repositories\ButtonRepositoryInterface',
            'App\Repositories\Eloquent\ButtonRepository'
        );
        $this->app->bind(
            'App\Repositories\ColorRepositoryInterface',
            'App\Repositories\Eloquent\ColorRepository'
        );
    }
}
