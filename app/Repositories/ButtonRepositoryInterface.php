<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;

interface ButtonRepositoryInterface
{
	public function updateConfiguration($id, Request $request);
}
