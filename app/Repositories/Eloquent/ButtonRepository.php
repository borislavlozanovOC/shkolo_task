<?php

namespace App\Repositories\Eloquent;

use App\Models\Button;

use App\Repositories\ButtonRepositoryInterface;

use Illuminate\Http\Request;

class ButtonRepository extends EloquentRepository implements ButtonRepositoryInterface
{
    /**
    * ButtonRepository constructor.
    *
    * @param Button $model
    */
    public function __construct(Button $model)
    {
        parent::__construct($model);
    }

    /**
    * @param $id
    * @param Request $request
    * @return Model
    */
    public function updateConfiguration($id, Request $request)
    {
    	$button = $this->update($id, $request->only('title'));

        if (!$button->configuration) {
            $button->configuration()->create($request->only(['color_id', 'hyperlink']));
        } else {
            $button->configuration()->update($request->only(['color_id', 'hyperlink']));
        }
    }
}
