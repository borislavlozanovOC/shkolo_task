<?php

namespace App\Repositories\Eloquent;

use Illuminate\Support\Collection;

use Illuminate\Database\Eloquent\Model;

use App\Repositories\EloquentRepositoryInterface;

class EloquentRepository implements EloquentRepositoryInterface
{
    /**
    * @var Model
    */
    protected $model;

    /**
    * EloquentRepository constructor.
    * 
    * @param Model $model
    */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
    * @return Collection
    */
    public function all(): Collection
    {
        return $this->model->all();    
    }

    /**
    * @param array $attributes
    * @return Model
    */
    public function create(array $attributes): Model
    {
        return $this->model->create($attributes);
    }

    /**
    * @param $id
    * @param array $attributes
    * @return Model
    */
    public function update($id, array $attributes): ?Model
    {
        $this->find($id)->update($attributes);

        return $this->find($id);
    }

    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model
    {
        return $this->model->find($id);
    }

    /**
    * @param $id
    * @return boolean
    */
    public function delete($id): bool
    {
        return $this->find($id)->delete();
    }
}
