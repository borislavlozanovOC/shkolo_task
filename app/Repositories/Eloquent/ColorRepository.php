<?php

namespace App\Repositories\Eloquent;

use App\Models\Color;

use App\Repositories\ColorRepositoryInterface;

class ColorRepository extends EloquentRepository implements ColorRepositoryInterface
{
    /**
    * ColorRepository constructor.
    *
    * @param Color $model
    */
    public function __construct(Color $model)
    {
        parent::__construct($model);
    }
}
