<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

use Illuminate\Database\Eloquent\Model;

/**
* Interface EloquentRepositoryInterface
*/
interface EloquentRepositoryInterface
{
    /**
    * @return Collection
    */
    public function all(): Collection;

    /**
    * @param array $attributes
    * @return Model
    */
    public function create(array $attributes): Model;

    /**
    * @param $id
    * @param array $attributes
    * @return Model
    */
    public function update($id, array $attributes): ?Model;

    /**
    * @param $id
    * @return Model
    */
    public function find($id): ?Model;

    /**
    * @param $id
    * @return boolean
    */
    public function delete($id): bool;
}
