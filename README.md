# README #

The README document whatever steps are necessary to get the application up and running.

### What is this repository for? ###

* Shkolo Task
* v.1

### Development Environment ###

The application was developed under ### Windows 10 ### using ### XAMP ###

### How do I get set up? ###

* Clone the repo
* #1 Run ### composer install ###
* #2 Copy ### .env.example ### into ### .env ### and make the necessary changes
* #3 Run ### php artisan migrate --seed ###

### Where can I see the application? ###

* The application was uploaded to AWS EC2 and can be checked from :
* * http://ec2-18-191-246-200.us-east-2.compute.amazonaws.com/ OR http://18.191.246.200/