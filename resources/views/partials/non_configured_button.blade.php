@if ($button)
<div class="text-center mb-4">
  <div class="d-inline-block">
    <div class="btn-toolbar mx-auto" role="toolbar">
      <div class="btn-group" role="group">
        <a href="{{ route('edit.button', $button) }}" class="btn btn-md btn-outline-danger">
          Configure Button
        </a>
      </div>
    </div>
  </div>
</div>
<div class="text-center">
  <div class="d-inline-block">
    <div class="btn-toolbar mx-auto" role="toolbar">
      <div class="btn-group" role="group">
        <button disabled type="button" class="btn btn-sm btn-outline-secondary">View</button>
        <button disabled type="button" class="btn btn-sm btn-outline-secondary">Edit</button>
      </div>
    </div>
  </div>
</div>
@endif