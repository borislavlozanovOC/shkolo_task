@if ($button)
<div class="text-center mb-4">
  <div class="d-inline-block">
    <div class="btn-toolbar mx-auto" role="toolbar">
      <div class="btn-group" role="group">
        <a href="{{ $button->configuration->hyperlink }}" target="_blank" class="btn btn-md btn-outline-primary">
          {{ str_limit($button->title, 15) }}
        </a>
      </div>
    </div>
  </div>
</div>
<div class="text-center">
  <div class="d-inline-block">
    <div class="btn-toolbar mx-auto" role="toolbar">
      <div class="btn-group" role="group">
        <a href="{{ route('edit.button', $button) }}" class="btn btn-sm btn-outline-warning">Edit</a>
        <form action="{{ route('destroy.button', $button) }}" method="POST">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <button type="submit" class="btn btn-sm btn-outline-danger">Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endif