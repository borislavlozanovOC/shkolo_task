@extends('layouts.app')

@section('content')

<main role="main">
  <div class="py-5 bg-light">
    <div class="container">
      
      <form class="text-center border border-light p-5 needs-validation" action="{{ route('update.button', $button) }}" method="post" novalidate>
        {{ method_field('put') }}
        {{ csrf_field() }}
        <p class="h4 mb-4">Configure button</p>
        
        <div class="col-xs-12">
          @if (count($errors) > 0)
            @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
              {{ $error }}  
            </div>
            @endforeach
          @endif
        </div>

        <div class="col-xs-12">
          <label for="title">Title</label>
          <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{ $button->title }}" required>
          <div class="valid-feedback">
            Looks good.
          </div>
          <div class="invalid-feedback">
            Please Enter Title!
          </div>
        </div>

        <div class="col-xs-12">
          <label for="hyperlink">Hyperlink</label>
          <input type="text" class="form-control" id="hyperlink" name="hyperlink" placeholder="Hyperlink" value="{{ $button->configuration ? $button->configuration->hyperlink : '' }}" required>
          <div class="valid-feedback">
            Looks good.
          </div>
          <div class="invalid-feedback">
            Please Enter Hyperlink!
          </div>
        </div>

        <div class="col-xs-12">
          <label for="color">Color</label>
          <select class="browser-default custom-select" name="color_id" id="color" required>
            <option value="" disabled>Choose option</option>
            @if ($colors)
            @foreach ($colors as $color)
              <option style="color:{{ $color->safe_color_name == 'white' ? 'black' : $color->safe_color_name }}" 
                value="{{ $color->id }}" {{ $button->configuration && $button->configuration->color_id == $color->id ? 'selected' : '' }}>
                {{ strtoupper($color->safe_color_name) }}
              </option>
            @endforeach
            @endif
          </select>
          <div class="valid-feedback">
            Looks good.
          </div>
          <div class="invalid-feedback">
            Please Choose Color!
          </div>
        </div>

        <button class="btn btn-info btn-block mt-4" type="submit">Save</button>
      </form>
    </div>
  </div>
</main>
@endsection
@section('component-scripts')
    <script src="{{asset('public/js/scripts.js')}}"></script>
@endsection