@extends('layouts.app')

@section('content')
<main role="main">
  <div class="py-5 bg-light">
    <div class="container">
      <div class="row">
        @if ($buttons)
        @foreach ($buttons as $button)
        <div class="col-md-4">
          <div class="card mb-4 box-shadow" style="border: {{ $button->configuration ? $button->configuration->color->safe_color_name : 'red' }} 10px solid;">
            <div class="card-body">
              @if ($button->configuration)
                @include('partials.configured_button', ['button' => $button])
              @else
                @include('partials.non_configured_button', ['button' => $button])
              @endif
            </div>
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</main>
@endsection
@section('component-scripts')
<!--  -->
@endsection